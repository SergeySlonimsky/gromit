<?php

namespace Application\Migrations;

use AppBundle\Entity\Menu;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20180908223911 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("INSERT INTO menu(name) VALUES(?)", [Menu::HEADER_MENU]);
        $this->addSql("INSERT INTO menu(name) VALUES(?)", [Menu::FOOTER_MENU]);
        $this->addSql("INSERT INTO menu(name) VALUES(?)", [Menu::SIDE_MENU]);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
    }
}
