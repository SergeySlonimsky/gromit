require('../css/media.scss');

$(document).ready(function () {
    const self = this;

    const
        mediaUrl = window.Routing.generate('admin_media_get_tree'),
        removeMediaUrl = window.Routing.generate('admin_media_remove_media'),
        removeFolderUrl = window.Routing.generate('admin_media_remove_folder'),
        mediaContainer = '#media-container',
        titleContainer = '#title-container',
        sizeContainer = '#size-container',
        gridContainer = '#grid-container',
        modal = '#myModal';
    let
        colSize = '1',
        folderId = null,
        contextMenuItems = {
            "add-media": "Add Media",
            "add-folder": "Add Folder",
        },
        tree = {};

    this.init = function () {
        this.getMediaList();
        this.handleActions();
    };

    this.getMediaList = function () {
        self.renderLoader();
        var urlParam = folderId ? '?folderId=' + folderId : '';
        fetch(mediaUrl + urlParam)
            .then(function (response) {
                if (response.status === 200) {
                    return response.json();
                } else {
                    throw response;
                }
            })
            .then(function (response) {
                tree = response;
                self.renderView();
            })
            .catch(function (error) {
                console.log(error);
                alert('error');
            });
    };

    this.renderView = function () {
        $(mediaContainer).html(
            '<div class="row">' +
            '   <div class="col-md-10" id="title-container"></div>' +
            '   <div class="col-md-2 text-right" id="size-container"></div>' +
            '</div>' +
            '<div class="row" id="grid-container"></div>' +
            '<i class="fas fa-4x fa-trash" id="trash"></i>'
        );
        self.renderOptions();
        self.renderGrid();
    };

    this.renderOptions = function () {
        let sizeSelector =
            '<select id="width-selector" class="form-control">' +
            '  <option value="4">3 items</option>' +
            '  <option value="3">4 items</option>' +
            '  <option value="2">6 items</option>' +
            '  <option value="1">12 items</option>' +
            '</select>';
        $(sizeContainer).html(sizeSelector);
        $(titleContainer).html('<h4>' + tree.folder.name + '</h4>');
        $('#width-selector').val(colSize);
    };

    this.renderGrid = function () {
        let view = '';
        let colClass = 'col-md-' + colSize;

        if (!tree) {
            return view;
        }

        if (tree.folder.parent) {
            view += '<div class="' + colClass + ' text-center media-folder p-2" data-id="'
                + tree.folder.parent.id
                + '"><img src="../../img/folder-icon.png" alt="" width="100%"><p><i class="fas fa-level-up-alt"></i> Up</p></div>';
        }

        tree.children.forEach(function (element) {
            view +=
                '<div class="' + colClass + ' text-center media-el media-folder p-2" ' +
                'draggable="true" ' +
                'data-id="' + element.id + '" ' +
                'data-name="' + element.name + '" ' +
                'data-type="folder" ' +
                '>' +
                '   <img src="../../img/folder-icon.png" alt="" width="100%">' +
                '   <p>' + element.name + '</p>' +
                '</div>';
        });

        tree.media.forEach(function (element) {
            view +=
                '<div class="' + colClass + ' text-center media-el media-image p-2" ' +
                'draggable="true" ' +
                'data-path="' + element.id + '" ' +
                'data-name="' + element.url + '" ' +
                'data-type="media" ' +
                '>' +
                '   <img src="' + element.url + '" alt="" width="100%">' +
                '</div>';
        });

        $(gridContainer).html(view);
    };

    this.renderLoader = function () {
        let loaderTopOffset = (window.innerHeight / 2) - 120;
        $(mediaContainer).html(
            '<div class="row text-center" style="margin-top:' + loaderTopOffset + 'px">' +
            '<div class="col-md-12">' +
            '<i class="mx-auto fas fa-circle-notch fa-spin fa-3x"></i>' +
            '</div>' +
            '</div>'
        );
    };

    this.backlightTrash = function (makeOn) {
        let trash = $('#trash');

        if (makeOn) {
            trash.addClass('backlight');
        } else {
            trash.removeClass('backlight');
        }
    };

    this.removeContextMenu = function () {
        let contextMenu = $('#context-menu');

        if (contextMenu.length) {
            contextMenu.remove();
        }
    };

    this.handleActions = function () {
        $(document).on('click', 'body', function () {
            self.removeContextMenu()
        });
        $(document).on('dblclick', '.media-folder', function () {
            folderId = $(this).attr('data-id');
            self.getMediaList();
        });
        $(document).on('change', '#width-selector', function () {
            colSize = this.value;
            self.renderGrid();
        });
        $(document).on('click', '.remove-media', function () {
            let id = $(this).attr('data-id');
            fetch(removemediaUrl + '?id=' + id)
                .then(() => self.getMediaList())
                .catch(() => alert('error'));
        });
        $(document).on('dragstart', '.media-el', function (e) {
            e.originalEvent.dataTransfer.setData('type', $(this).attr('data-type'));
            e.originalEvent.dataTransfer.setData('name', $(this).attr('data-name'));
            e.originalEvent.dataTransfer.setData('path', $(this).attr('data-path'));
            self.backlightTrash(true);
        });
        $(document).on('dragend', '.media-el', function () {
            self.backlightTrash(false);
        });
        $(document).on('dragover', '#trash', function (e) {
            e.preventDefault();
        });
        $(document).on('drop', '#trash', function (e) {
            e.preventDefault();
            if (confirm('You really want to delete ' + e.originalEvent.dataTransfer.getData('name'))) {
                if (e.originalEvent.dataTransfer.getData('type') === 'media') {
                    fetch(removeMediaUrl + '?id=' + e.originalEvent.dataTransfer.getData('path'))
                        .then(function () {
                            self.getMediaList();
                        });
                } else {
                    alert('Can\'t delete folder');
                }
            }
        });
        $(document).on('contextmenu', '#content-wrapper', function (e) {
            e.preventDefault();
            self.removeContextMenu();
            let menuList = '';

            for (let key in contextMenuItems) {
                menuList += '<li class="context-item" data-toggle="modal" data-target="#' + key + '">' + contextMenuItems[key] + '</li>';
            }

            let menu =
                '<div id="context-menu" style="left:' + e.pageX + 'px; top:' + e.pageY + 'px;">' +
                '   <ul>' + menuList + '</ul>' +
                '</div>';
            $(this).append(menu);
        });
    };

    this.init();
});
