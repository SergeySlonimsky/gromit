$(document).ready(function () {
    const
        self = this,
        url = window.Routing.generate('mail_send'),
        thankUrl = window.Routing.generate('mail_thank'),
        examplesUrl = window.Routing.generate('app_examples'),
        exampleContainer = '#example-container',
        examplesBtn = '#more-examples';

    let exOffset = 0;

    this.init = function () {
        self.getExamples();
        $(document).on('click', '.filled-in', function () {
            console.log('event');
            if ($(this).prop('checked')) {
                $('.' + $(this).attr('id')).show();
            } else {
                $('.' + $(this).attr('id')).hide();
            }
        });
        $(document).on('submit', '.contact-form', function (e) {
            e.preventDefault();
            let btn = $(this).find('button');
            let btnData = btn.html();
            console.log(btn, btnData);
            btn.html('<i class="fa fa-spinner fa-spin"></i>');

            let data = $(this).serializeArray();

            $.ajax({
                url: url,
                data: JSON.stringify(data),
                contentType: false,
                type: 'POST',
                success: function () {
                    location.href = thankUrl;
                    btn.html(btnData);
                },
                error: function () {
                    btn.html(btnData);
                    alert('Ошибка при отправке, попробуйте еще раз');
                }
            });
        });
        $(document).on('click', '#more-examples', function () {
            $(this).html('<i class="fa fa-spinner fa-spin"></i>');
            self.getExamples();
        })
    };

    this.getExamples = function () {
        fetch(examplesUrl + '?offset=' + exOffset)
            .then(response => {
                return response.json();
            })
            .then(data => {
                exOffset += 4;
                data.forEach(function (value) {
                    let render =
                        '<div class="col-md-6">' +
                        '<div class="shadowed service-block text-left">' +
                        '<div class="example-image" style="background-image:url('+value.url+');"></div>' +
                        '<p style="margin-top: 0.5em;">'+value.title+'</p></div>' +
                        '</div>';
                    $(exampleContainer).append(render);
                });
                $(examplesBtn).html('больше примеров');
            })
            .catch(error => {
                console.log(error);
                $(examplesBtn).html('больше примеров');
            });
    };

    this.init();
});
