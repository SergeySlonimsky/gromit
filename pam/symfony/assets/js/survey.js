require('../css/survey.scss');
let Sortable = require('sortablejs');

$(document).ready(function () {
    const
        self = this,
        url = window.Routing.generate('admin_settings_survey_update'),
        removeUrl = window.Routing.generate('admin_settings_survey_remove'),
        addButton = '#survey-add-q',
        addForm = '#add-question-form';

    let el = document.getElementById('items');
    Sortable.create(el, {
        filter: '.js-remove',
        onFilter: function (evt) {
            let el = editableList.closest(evt.item);
            el && el.parentNode.removeChild(el);
        }
    });

    this.showInput = function(show) {
        if (show) {
            $(addButton).hide();
            $(addForm).show();
        } else {
            $(addButton).show();
            $(addForm).hide();
        }
    };

    $(document).on('click', addButton, function () {
        self.showInput(true);
    });
    $(document).on('submit', addForm, function (e) {
        e.preventDefault();
        let text = $('#add-question-input').val();

        $('#add-question-input').value = '';
        $('#items').append(
            '<li class="list-group-item"><a href="#" onclick="alert(\'Сохраните, прежде чем продолжить\');">' + text + '</a><i class="fa fa-times float-right"></i></li>'
        );
        self.showInput(false);
    });
    $(document).on('click', '.remove-question', function () {
        fetch(removeUrl)
            .then(response => {
                $(this).parent().remove();
            })
            .catch(error => {
                alert('Ошибка');
            });
    });
    $(document).on('click', '#save', function (e) {
        e.preventDefault();

        let result = [];

        $('#items').find('li').each(function (key, value) {
            result.push({
                'id': $(value).attr('data-id'),
                'text': value.innerText,
                'priority': key
            });
        });
        $.post({
            url: url,
            data: {
                'result': result,
            }
        }).done(function () {
            location.reload();
        }).fail(function () {
            alert('Ошибка');
        });
    });
});
