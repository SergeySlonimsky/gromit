require('../css/global.scss');

window.$ = require('jquery');
window.jQuery = window.$;
window.Popper = require('popper.js');
require('jquery-backstretch');
require('bootstrap');
require('./admin/sb-admin.min');
require('../select2/select2.full.min');
require('../select2/select2entity');

const routes = require('../../web/js/fos_js_routes.json');
import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';

Routing.setRoutingData(routes);
window.Routing = Routing;
const mediaUrl = window.Routing.generate('app_get_main_images');

$('.dropdown-menu a.dropdown-toggle').on('click', function (e) {
    if (!$(this).next().hasClass('show')) {
        $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
    }
    let $subMenu = $(this).next(".dropdown-menu");
    $subMenu.toggleClass('show');


    $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
        $('.dropdown-submenu .show').removeClass("show");
    });


    return false;
});

$(".grid-column-description").each(function () {
    if ($(this).text().length > 50)
        $(this).text($(this).text().substring(0, 50) + '...');
});

$('.grid_body').find('table').addClass('table table-striped table-bordered table-sm');

let imageCounter = 1;

/*function myLoop() {
    setTimeout(function () {
        $('.main-screen-index').delay(300).fadeTo('slow', .3, function () {
            $(this).css('background-image', 'url(/img/main/img/slider/'+imageCounter+'.jpg)');
        }).delay(300).fadeTo('slow', 1);
        if (imageCounter < 6) {
            imageCounter++;
        } else {
            imageCounter = 1;
        }
        myLoop();
    }, 3000)
}

myLoop();*/
fetch(mediaUrl)
    .then(response => {
        return response.json();
    })
    .then(data => {
        $('.main-screen-index').backstretch(data, {
            fade: 750,
            duration: 4000
        });
    })
    .catch(error => {
        console.log(error);
        $('.main-screen-index').backstretch([
            "/img/main/img/slider/1.jpg",
            "/img/main/img/slider/2.jpg",
            "/img/main/img/slider/3.jpg",
            "/img/main/img/slider/4.jpg",
            "/img/main/img/slider/5.jpg",
            "/img/main/img/slider/6.jpg",
            "/img/main/img/slider/7.jpg",
        ], {
            fade: 750,
            duration: 4000
        });
    });

