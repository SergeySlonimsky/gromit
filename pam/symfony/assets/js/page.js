require('../css/page.scss');
require('lightbox2');

$(document).ready(function () {
    const
        self = this,
        galleryContainer = '#gallery-container',
        imageForm = '#image-form',
        imageSelector = '#page_form_media',
        deleteImageSelector = '.delete-image',
        pageId = $(galleryContainer).attr('data-page'),
        getGalleryUrl = window.Routing.generate('admin_media_get_media'),
        deleteMediaUrl = window.Routing.generate('admin_media_delete_media'),
        uploadMediaUrl = window.Routing.generate('admin_media_upload_media'),
        setMainUrl = window.Routing.generate('admin_media_set_main');
    let
        loader = false;

    this.init = function () {
        console.log(pageId);
        self.getGallery();
        self.handleActions();
    };

    this.getGallery = function () {
        fetch(getGalleryUrl + '/' + pageId)
            .then(response => {
                return response.json();
            })
            .then(response => {
                response.forEach(function (value) {
                    self.addToGallery(value);
                });
            })
            .catch();
    };

    this.addToGallery = function (item) {
        let main = item.isMain
            ? '<a class="btn btn-sm btn-outline-info main-img-btn disabled">Главное</a>'
            : '<a class="btn btn-sm btn-info main-img-btn main-img-btn-makeble" data-id="' + item.id + '">Сделать главным</a>';
        $(galleryContainer).append(
            '<div class="col-md-2">' +
            '<a href="../../../img/main/uploads/' + item.url + '" data-lightbox="gallery">' +
            '<img src="../../../img/main/uploads/' + item.url + '" alt="Not Found" width="100%">' +
            '</a>' +
            main +
            '<p class="text-center"><i data-id="' + item.id + '" class="fa fa-times delete-image"></i></p>' +
            '</div>'
        );
    };

    this.handleActions = function () {
        $(document).on('submit', imageForm, function (e) {
            e.preventDefault();

            let data = new FormData();
            let files = $("#image-form input[type='file']")[0].files;

            if (!files.length) {
                alert('Выберите изображения');
                return
            }

            $.each(files, function (i, file) {
                data.append(i, file);
            });

            fetch(uploadMediaUrl, {
                method: 'POST',
                body: data,
            }).then(response => {
                return response.json();
            }).then(data => {
                console.log(data);
                data.forEach(function (value) {
                    $(imageSelector).append(
                        '<option value="' + value.id + '" selected="selected">' + value.url + '</option>'
                    );
                    self.addToGallery(value);
                });
            }).catch(error => {
                console.log(error);
                alert('Oops! Something wrong');
            })
        });
        $(document).on('click', deleteImageSelector, function () {
            let id = $(this).attr('data-id');

            if (confirm("Вы действительно хотиите удалить изобрадение?")) {
                fetch(deleteMediaUrl + '/' + id)
                    .then(() => {
                        $(this).parent().parent().remove();
                        $('#page_form_media option[value=' + id + ']').remove();
                    })
                    .catch(error => {
                        alert(error.toString());
                        console.log(error);
                    });
            }
        });
        $(document).on('click', '.main-img-btn-makeble', function () {
            fetch(setMainUrl + '/' + $(this).attr('data-id'))
                .then(() => {
                    $(galleryContainer).html('');
                    self.getGallery();
                })
                .catch(error => {
                    console.log(error);
                    alert('Ошибка');
                    $(galleryContainer).html('');
                    self.getGallery();
                });

        });
    };

    self.init();
});

