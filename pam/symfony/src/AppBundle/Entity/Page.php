<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Media\Media;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use APY\DataGridBundle\Grid\Mapping as GRID;
use Doctrine\ORM\PersistentCollection;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PageRepository")
 * @ORM\Table(name="page",uniqueConstraints={@ORM\UniqueConstraint(name="slug", columns={"slug"})})
 */
class Page
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @GRID\Column(visible=false)
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     *
     * @GRID\Column(title="Title", operatorsVisible=false)
     */
    private $title;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true, options={"default":""})
     *
     * @GRID\Column(visible=false)
     */
    private $description = '';

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true, options={"default":""})
     *
     * @GRID\Column(visible=false)
     */
    private $content = '';

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     *
     * @GRID\Column(title="Slug", operatorsVisible=false)
     */
    private $slug;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     *
     * @GRID\Column(title="Is Visible", operatorsVisible=false, size="100")
     */
    private $isVisible;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     *
     * @GRID\Column(title="Show previews", operatorsVisible=false, size="100")
     */
    private $showPreviews;

    /**
     * @var Page|null
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Page", inversedBy="subPages")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     *
     * @GRID\Column(field="parent.title", title="Parent Page", operatorsVisible=false)
     */
    private $parent;

    /**
     * @var Collection|null
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Page", mappedBy="parent")
     */
    private $subPages;

    /**
     * @var Collection|null
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Media\Media", mappedBy="page")
     */
    private $media;


    public function __construct()
    {
        $this->isVisible = false;
        $this->showPreviews = false;
        $this->subPages = new ArrayCollection();
        $this->media = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Page
     */
    public function setId(int $id): Page
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Page
     */
    public function setTitle(string $title): Page
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     * @return Page
     */
    public function setDescription(?string $description): Page
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param null|string $content
     * @return Page
     */
    public function setContent(?string $content): Page
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @param string|null $slug
     * @return Page
     */
    public function setSlug(?string $slug): Page
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return bool
     */
    public function isVisible(): bool
    {
        return $this->isVisible;
    }

    /**
     * @param bool $isVisible
     * @return Page
     */
    public function setIsVisible(bool $isVisible): Page
    {
        $this->isVisible = $isVisible;
        return $this;
    }

    /**
     * @return bool
     */
    public function isShowPreviews(): bool
    {
        return $this->showPreviews;
    }

    /**
     * @param bool $showPreviews
     * @return Page
     */
    public function setShowPreviews(bool $showPreviews): Page
    {
        $this->showPreviews = $showPreviews;
        return $this;
    }

    /**
     * @return Page|null
     */
    public function getParent(): ?Page
    {
        return $this->parent;
    }

    /**
     * @param Page|null $parent
     * @return Page
     */
    public function setParent(?Page $parent): Page
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return PersistentCollection|null
     */
    public function getSubPages(): ?PersistentCollection
    {
        return $this->subPages;
    }

    /**
     * @param Page $page
     * @return Page
     */
    public function removeSubPage(Page $page): Page
    {
        $this->subPages->removeElement($page);

        return $this;
    }

    /**
     * @param Page $page
     * @return Page
     */
    public function addSubPage(Page $page): Page
    {
        $this->subPages->add($page);

        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getMedia(): ?Collection
    {
        return $this->media;
    }

    /**
     * @param Collection|null $media
     * @return Page
     */
    public function setMedia(?Collection $media): Page
    {
        $this->media = $media;
        return $this;
    }

    /**
     * @param Media $media
     * @return Page
     */
    public function addMedia(Media $media): Page
    {
        $this->media->add($media);
        $media->setPage($this);

        return $this;
    }

    /**
     * @param Media $media
     * @return Page
     */
    public function removeMedia(Media $media): Page
    {
        $this->media->removeElement($media);

        return $this;
    }

}
