<?php

namespace AppBundle\Entity\Survey;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use APY\DataGridBundle\Grid\Mapping as GRID;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\QuestionRepository")
 * @ORM\Table(name="survey_question")
 */
class Question
{
    const TYPES = [
        'peregorodki' => 'Рассчитать перегородки',
        'ograzhdeniya' => 'Рассчитать ограждения',
    ];

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @GRID\Column(title="Id", operatorsVisible=false)
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     *
     * @GRID\Column(title="Name", operatorsVisible=false)
     */
    private $question;

    /**
     * @var string|null
     *
     * @ORM\Column(type="integer")
     *
     * @GRID\Column(title="Name", operatorsVisible=false)
     */
    private $priority;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=TRUE)
     *
     * @GRID\Column(title="Name", operatorsVisible=false)
     */
    private $type;

    /**
     * @var Collection|null
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Survey\Answer", mappedBy="question", orphanRemoval=true, cascade={"persist"})
     */
    private $answers;

    public function __construct()
    {
        $this->answers = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Question
     */
    public function setId(int $id): Question
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getQuestion(): ?string
    {
        return $this->question;
    }

    /**
     * @param null|string $question
     * @return Question
     */
    public function setQuestion(?string $question): Question
    {
        $this->question = $question;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getPriority(): ?string
    {
        return $this->priority;
    }

    /**
     * @param null|string $priority
     * @return Question
     */
    public function setPriority(?string $priority): Question
    {
        $this->priority = $priority;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param null|string $type
     * @return Question
     */
    public function setType(?string $type): Question
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getAnswers(): ?Collection
    {
        return $this->answers;
    }

    /**
     * @param Collection|null $answers
     * @return Question
     */
    public function setAnswers(?Collection $answers): Question
    {
        $this->answers = $answers;
        return $this;
    }

    /**
     * @param Answer $answer
     * @return $this
     */
    public function addAnswer(Answer $answer)
    {
        $this->answers->add($answer);
        return $this;
    }

    /**
     * @param Answer $answer
     * @return $this
     */
    public function removeAnswer(Answer $answer)
    {
        $this->answers->removeElement($answer);
        return $this;
    }
}
