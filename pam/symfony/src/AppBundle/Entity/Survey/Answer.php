<?php

namespace AppBundle\Entity\Survey;

use Doctrine\ORM\Mapping as ORM;
use APY\DataGridBundle\Grid\Mapping as GRID;

/**
 * @ORM\Entity()
 * @ORM\Table(name="survey_answer")
 */
class Answer
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @GRID\Column(title="Id", operatorsVisible=false)
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     *
     * @GRID\Column(title="Name", operatorsVisible=false)
     */
    private $answer;

    /**
     * @var Question|null
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Survey\Question", inversedBy="answers")
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     */
    private $question;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Answer
     */
    public function setId(int $id): Answer
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getAnswer(): ?string
    {
        return $this->answer;
    }

    /**
     * @param null|string $answer
     * @return Answer
     */
    public function setAnswer(?string $answer): Answer
    {
        $this->answer = $answer;
        return $this;
    }

    /**
     * @return Question|null
     */
    public function getQuestion(): ?Question
    {
        return $this->question;
    }

    /**
     * @param Question|null $question
     * @return Answer
     */
    public function setQuestion(?Question $question): Answer
    {
        $this->question = $question;
        return $this;
    }
}
