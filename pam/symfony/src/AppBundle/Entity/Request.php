<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use APY\DataGridBundle\Grid\Mapping as GRID;

class Request
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @GRID\Column(visible=false)
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     *
     * @GRID\Column(title="Name", operatorsVisible=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     *
     * @GRID\Column(title="From", operatorsVisible=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     *
     * @GRID\Column(title="Phone", operatorsVisible=false)
     */
    private $phone;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     *
     * @GRID\Column(title="Message", operatorsVisible=false)
     */
    private $message;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     *
     * @GRID\Column(title="Created At", operatorsVisible=false)
     */
    private $created_at;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Request
     */
    public function setId(int $id): Request
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Request
     */
    public function setName(string $name): Request
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Request
     */
    public function setEmail(string $email): Request
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return Request
     */
    public function setPhone(string $phone): Request
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param null|string $message
     * @return Request
     */
    public function setMessage(?string $message): Request
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->created_at;
    }

    /**
     * @param \DateTime $created_at
     * @return Request
     */
    public function setCreatedAt(\DateTime $created_at): Request
    {
        $this->created_at = $created_at;

        return $this;
    }
}
