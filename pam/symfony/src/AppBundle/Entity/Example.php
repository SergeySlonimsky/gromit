<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use APY\DataGridBundle\Grid\Mapping as GRID;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ExampleRepository")
 * @ORM\Table(name="example")
 */
class Example
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @GRID\Column(visible=false)
     */
    protected $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     *
     * @GRID\Column(title="Пример работы", operatorsVisible=false)
     */
    private $title;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Media\Media")
     * @ORM\JoinColumn(name="media_id", referencedColumnName="id")
     *
     * @GRID\Column(visible=false)
     */
    private $image;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Example
     */
    public function setId(int $id): Example
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param null|string $title
     * @return Example
     */
    public function setTitle(?string $title): Example
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     * @return Example
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }
}