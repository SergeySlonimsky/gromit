<?php

namespace AppBundle\Service\Extension;

use AppBundle\Entity\Settings;
use AppBundle\Repository\SettingsRepository;
use Doctrine\ORM\EntityManagerInterface;

class MenuExtension extends \Twig_Extension
{
    /**
     * @var \Twig_Environment
     */
    private $environment;

    /**
     * @var SettingsRepository
     */
    private $repository;

    public function __construct(\Twig_Environment $environment, EntityManagerInterface $em)
    {
        $this->environment = $environment;
        $this->repository = $em->getRepository(Settings::class);
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('renderFooter', [$this, 'renderFooter']),
            new \Twig_SimpleFunction('renderHeader', [$this, 'renderHeader']),
        );
    }

    public function renderHeader()
    {
        return $this->environment->render('site/partials/header.html.twig', [
            'items' => $this->repository->getPhones(),
        ]);
    }

    public function renderFooter()
    {
        return $this->environment->render('site/partials/footer.html.twig', [
            'items' => $this->repository->getPhones(),
        ]);
    }
}
