<?php

namespace AppBundle\Service\Helper;

use AppBundle\Entity\Media\Media;
use AppBundle\Entity\Page;
use AppBundle\Exception\ApiException;
use AppBundle\Repository\MediaRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;

class MediaHelper
{
    const IMAGE_ALLOWED_TYPES = [
        'image/png',
        'image/jpeg',
    ];

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var MediaRepository
     */
    private $mediaRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
        $this->mediaRepository = $entityManager->getRepository(Media::class);
    }

    public function getMediaList(string $pageId)
    {
        return $this->mediaRepository->getMediaGallery($pageId);
    }

    /**
     * @param UploadedFile $image
     * @return Media
     * @throws ApiException
     */
    public function uploadImage(UploadedFile $image, string $dir): Media
    {
        if ($image->getMimeType() && in_array($image->getMimeType(), self::IMAGE_ALLOWED_TYPES)) {
            try {
                $filename = md5(uniqid()) . '.' . $image->guessExtension();
                $image->move($dir, $filename);
                $media = new Media();
                $media->setUrl($filename);
                $media->setIsMain(false);
                $this->em->persist($media);
                $this->em->flush();

                return $media;
            } catch (\Exception $e) {
                throw new ApiException(sprintf('Can\'t save %s image, internal error', $image->getFilename()), Response::HTTP_INTERNAL_SERVER_ERROR);
            }

        } else {
            throw new ApiException(sprintf("Extension of file: %s is not allowed. Use .png or .jpg images", $image->getFilename()), Response::HTTP_BAD_REQUEST);
        }
    }


    /**
     * @param string $id
     * @throws ApiException
     */
    public function deleteImage(string $id)
    {
        try {
            $this->mediaRepository->removeMedia($id);
        } catch (\Exception $e) {
            throw new ApiException('Can\'t delete image, internal error', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param string $id
     */
    public function setMain(string $id)
    {
        $image = $this->mediaRepository->find($id);
        /** @var Page $page */
        $page = $image->getPage();
        foreach ($page->getMedia() as $media) {
            $media->setIsMain(false);
        }
        $image->setIsMain(true);
        $this->em->flush();
    }
}
