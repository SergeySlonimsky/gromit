<?php

namespace AppBundle\Util;

class FormUtil
{
    const SAVE_BTN_OPTS = [
        'label' => 'Сохранить',
        'attr' => [
            'class' => 'btn btn-primary',
        ]
    ];
}
