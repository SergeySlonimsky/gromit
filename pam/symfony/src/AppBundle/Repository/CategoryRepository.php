<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class CategoryRepository extends EntityRepository
{
    public function getAllByQuery(string $query)
    {
        $qb = $this->createQueryBuilder('c');

        return $qb
            ->where($qb->expr()->like('c.name', $qb->expr()->literal('%'.$query.'%')))
            ->getQuery()->getResult();
    }
}
