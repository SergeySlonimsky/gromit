<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Survey\Question;
use Doctrine\ORM\EntityRepository;

class QuestionRepository extends EntityRepository
{
    public function getQuestions()
    {
        return $this
            ->createQueryBuilder('q')
            ->orderBy('q.priority', 'ASC')
            ->getQuery()->getResult();
    }

    public function updateQuestion(array $question)
    {
        if (!isset($question['id'])) {
            $this->createQuestion($question['text'], $question['priority']);
        } else {
            /** @var Question $q */
            $q = $this->find($question['id']);

            if ($q) {
                $q->setQuestion($question['text']);
                $q->setPriority($question['priority']);
                $this->getEntityManager()->persist($q);
            }
        }
    }

    public function createQuestion(string $text, string $priority)
    {
        $q = new Question();
        $q->setQuestion($text);
        $q->setPriority($priority);
        $this->getEntityManager()->persist($q);
    }
}
