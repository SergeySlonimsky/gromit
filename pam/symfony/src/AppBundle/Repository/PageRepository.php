<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Page;
use Doctrine\ORM\EntityRepository;

class PageRepository extends EntityRepository
{
    /**
     * @param string $slug
     *
     * @return Page
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getPageBySlug(string $slug): Page
    {
        $qb = $this->createQueryBuilder('p')
            ->where('p.slug = ?1')
            ->andWhere('p.isVisible = TRUE')
            ->setParameter('1', $slug);

        return $qb->getQuery()->getSingleResult();
    }

    public function getByQuery(string $query)
    {
        $qb = $this->createQueryBuilder('p');

        return $qb
            ->where('p.isVisible = TRUE')
            ->andWhere($qb->expr()->like('p.title', $qb->expr()->literal('%'.$query.'%')))
            ->getQuery()->getResult();
    }

    public function getPagesByParent(string $parentId = null)
    {
        $qb = $this->createQueryBuilder('p')
            ->where('p.isVisible = TRUE');
        if ($parentId) {
            $qb
                ->andWhere('p.parent = :parentId')
                ->setParameter('parentId', $parentId);
        } else {
            $qb
                ->andWhere('p.parent IS NULL');
        }

        return $qb->getQuery()->getResult();
    }

    public function getMedia(string $id)
    {
        return $this->createQueryBuilder('p')
            ->select('p.media')
            ->where('p.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }
}
