<?php

namespace AppBundle\Repository;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;

class ExampleRepository extends EntityRepository
{
    public function getExamplePart(string $offset = null)
    {
        $qb = $this->createQueryBuilder('e')
            ->select(['e.title', 'image.url'])
            ->innerJoin('e.image', 'image')
            ->setMaxResults(4);
        if ($offset)
        {
            $qb->setFirstResult( $offset );
        }

        return $qb->getQuery()->getResult(AbstractQuery::HYDRATE_ARRAY);
    }
}
