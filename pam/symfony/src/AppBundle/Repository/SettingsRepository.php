<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class SettingsRepository extends EntityRepository
{
    public function getPhones()
    {
        $qb = $this->createQueryBuilder('s');

        return $qb
            ->where('s.name IN (:names)')
            ->setParameter('names', ['phone1', 'phone2', 'phone3'])
            ->getQuery()
            ->getResult();
    }
}
