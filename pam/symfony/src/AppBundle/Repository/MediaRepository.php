<?php

namespace AppBundle\Repository;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;

class MediaRepository extends EntityRepository
{
    public function removeMedia(string $id)
    {
        return $this->createQueryBuilder('m')
            ->delete('AppBundle:Media\Media', 'm')
            ->where('m.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }

    public function getMediaGallery(string $page)
    {
        return $this->createQueryBuilder('m')
            ->where('m.page = :id')
            ->setParameter('id', $page)
            ->getQuery()
            ->getResult(AbstractQuery::HYDRATE_ARRAY);
    }
}
