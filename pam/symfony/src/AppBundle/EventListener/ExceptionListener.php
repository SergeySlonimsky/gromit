<?php

namespace AppBundle\EventListener;

use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\RouterInterface;

class ExceptionListener
{
    private $router;

    public function __construct(RouterInterface $router) {
        $this->router = $router;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if ($exception instanceof NotFoundHttpException || $exception instanceof ORMException) {
            if ($event->getRequest()->get('_route') == 'not-found') {
                return;
            }

            $url = $this->router->generate('app_not_found');

            $response = new RedirectResponse($url);
            $event->setResponse($response);
        }
    }
}
