<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Media\Media;
use AppBundle\Entity\Page;
use AppBundle\Exception\ApiException;
use AppBundle\Repository\PageRepository;
use AppBundle\Service\Helper\MediaHelper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MediaController extends Controller
{
    /** @var MediaHelper */
    private $mediaHelper;

    public function __construct(MediaHelper $mediaHelper)
    {
        $this->mediaHelper = $mediaHelper;
    }

    public function indexAction()
    {
        return $this->render('admin/media/index.html.twig');
    }

    public function uploadMediaAction(Request $request)
    {
        $result = [];
        $imageDir = $this->getParameter('image-dir');

        try {
            foreach ($request->files->all() as $file) {
                $media = $this->mediaHelper->uploadImage($file, $imageDir);
                $result[] = [
                    "id" => $media->getId(),
                    "url" => $media->getUrl(),
                ];
            }
            return new JsonResponse($result, Response::HTTP_OK);
        } catch (ApiException $e) {
            return new JsonResponse($e->getMessage(),$e->getCode());
        }
    }

    public function getMediaAction(string $id)
    {
        $media = $this->mediaHelper->getMediaList($id);

        return new JsonResponse($media, Response::HTTP_OK);
    }

    public function deleteMediaItemAction(string $id)
    {
        try {
            $this->mediaHelper->deleteImage($id);
            return new JsonResponse(null, Response::HTTP_OK);
        } catch (ApiException $e) {
            return new JsonResponse($e->getMessage(),$e->getCode());
        }
    }

    public function setMainAction(string $id)
    {
        try {
            $this->mediaHelper->setMain($id);
            return new JsonResponse(null, Response::HTTP_OK);
        } catch (\Exception $e) {
            return new JsonResponse($e->getMessage(),$e->getCode());
        }
    }
}
