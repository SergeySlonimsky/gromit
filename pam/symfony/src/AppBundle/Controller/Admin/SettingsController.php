<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Menu;
use AppBundle\Entity\Settings;
use AppBundle\Entity\Survey\Answer;
use AppBundle\Entity\Survey\Question;
use AppBundle\Form\MenuCollectionFormType;
use AppBundle\Form\QuestionFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SettingsController extends Controller
{
    /** @var EntityManagerInterface */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function generalAction(Request $request)
    {
        $settingsRepo = $this->em->getRepository(Settings::class);

        $settings = $settingsRepo->findAll();

        if ($request->getMethod() == Request::METHOD_POST) {
            foreach ($request->request as $key => $value) {
                $settingsRepo->find($key)->setValue($value);
            }

            $this->em->flush();

            return $this->redirectToRoute('admin_settings_general');
        }

        return $this->render('admin/settings/general.html.twig', [
            'settings' => $settings,
        ]);
    }

    public function surveyAction(Request $request)
    {
        $questions = $this->get('doctrine.orm.default_entity_manager')
            ->getRepository(Question::class)
            ->getQuestions();
        return $this->render('admin/settings/survey.html.twig', [
            'questions' => $questions,
        ]);
    }

    public function updateSurveyAction(Request $request)
    {
        $result = $request->get('result');
        $repository = $this->get('doctrine.orm.default_entity_manager')->getRepository(Question::class);
        foreach ($result as $item) {
            $repository->updateQuestion($item);
        }
        $this->get('doctrine.orm.default_entity_manager')->flush();

        return new JsonResponse(Response::HTTP_OK);
    }

    public function removeQuestionAction(Question $question)
    {
        $this->em->remove($question);

        return new JsonResponse('OK');
    }

    public function viewQuestionAction(Request $request, Question $question)
    {
        $form = $this->createForm(QuestionFormType::class, $question);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Question $question */
            $question = $form->getData();
            foreach ($question->getAnswers() as $answer) {
                /** @var $answer Answer */
                $answer->setQuestion($question);
            }

            $this->em->persist($question);
            $this->em->flush();
        }

        return $this->render('admin/settings/question.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
