<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Category;
use AppBundle\Entity\Page;
use AppBundle\Entity\Product;
use AppBundle\Form\CategoryFormType;
use AppBundle\Form\ProductFormType;
use APY\DataGridBundle\Grid\Action\RowAction;
use APY\DataGridBundle\Grid\Source\Entity;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends Controller
{
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
       $this->em = $em;
    }

    public function productListAction()
    {
        $source = new Entity(Product::class);
        $grid = $this->get('grid');
        $grid->setSource($source);
        $rowActionEdit = new RowAction('Edit', 'admin_product_edit', false);
        $rowActionEdit->setRouteParameters(['id']);
        $grid->addRowAction($rowActionEdit);

        return $grid->getGridResponse('admin/product/list.html.twig');
    }

    public function productItemAction(Request $request, Product $product = null)
    {
        if (!$product) {
            $product = new Product();
        }

        $form = $this->createForm(ProductFormType::class, $product);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Page $page */
            $product = $form->getData();
            $this->em->persist($product);
            $this->em->flush();
        }
        return $this->render('admin/product/item.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function categoryListAction()
    {
        $source = new Entity(Category::class);
        $grid = $this->get('grid');
        $grid->setSource($source);
        $rowActionEdit = new RowAction('Edit', 'admin_category_edit', false);
        $rowActionEdit->setRouteParameters(['id']);
        $grid->addRowAction($rowActionEdit);

        return $grid->getGridResponse('admin/product/category/list.html.twig');
    }

    public function categoryItemAction(Request $request, Category $category = null)
    {
        if (!$category) {
            $category = new Category();
        }

        $form = $this->createForm(CategoryFormType::class, $category);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Page $page */
            $page = $form->getData();
            $this->em->persist($page);
            $this->em->flush();
        }
        return $this->render('admin/product/category/item.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function getCategoryByQueryAction(Request $request)
    {
        $pages = $this->em->getRepository(Category::class)->getAllByQuery($request->get('q'));
        $result = [];

        array_map(function ($value) use (&$result){
            /** @var $value Category */
            $result[] = [
                'id' => $value->getId(),
                'text' => $value->getName(),
            ];
        }, $pages);

        return new JsonResponse($result, Response::HTTP_OK);
    }
}
