<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Faq;
use AppBundle\Form\FaqFormType;
use APY\DataGridBundle\Grid\Action\RowAction;
use APY\DataGridBundle\Grid\Source\Entity;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FaqController extends Controller
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var EntityRepository */
    private $faqRepository;

    /**
     * FaqController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->faqRepository = $em->getRepository(Faq::class);
    }

    /**
     * @return Response
     */
    public function listAction(): Response
    {
        $source = new Entity(Faq::class);
        $grid = $this->get('grid');
        $grid->setSource($source);
        $rowActionEdit = new RowAction('Редактировать', 'admin_faq_edit', false);
        $rowActionEdit->setAttributes(['class' => 'btn btn-primary btn-xs m-1']);
        $rowActionEdit->setRouteParameters(['id']);
        $rowActionDelete = new RowAction('Удалить', 'admin_faq_delete', true);
        $rowActionDelete->setRouteParameters(['id']);
        $rowActionDelete->setAttributes(['class' => 'btn btn-danger btn-xs m-1']);
        $grid->addRowAction($rowActionEdit);
        $grid->addRowAction($rowActionDelete);

        return $grid->getGridResponse('admin/faq/index.html.twig');
    }

    /**
     * @param Request $request
     * @param Faq|null $faq
     * @return Response
     */
    public function itemAction(Request $request, Faq $faq = null): Response
    {
        $faq = $faq ?: new Faq();
        $form = $this->createForm(FaqFormType::class, $faq);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Faq $faq */
            $faq = $form->getData();
            $this->em->persist($faq);
            $this->em->flush();

            return $this->redirectToRoute('admin_faq_list');
        }

        return $this->render('admin/faq/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Faq $faq
     * @return Response
     */
    public function deleteAction(Faq $faq): Response
    {
        $this->em->remove($faq);
        $this->em->flush();

        return $this->redirectToRoute('admin_faq_list');
    }
}
