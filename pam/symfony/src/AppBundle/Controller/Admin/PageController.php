<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Example;
use AppBundle\Entity\Page;
use AppBundle\Form\ExampleFormType;
use AppBundle\Form\PageFormType;
use APY\DataGridBundle\Grid\Action\RowAction;
use APY\DataGridBundle\Grid\Source\Entity;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PageController extends Controller
{
    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function indexAction()
    {
        $source = new Entity(Page::class);
        $grid = $this->get('grid');
        $grid->setSource($source);
        $rowActionEdit = new RowAction('Редактировать', 'admin_page_edit', false);
        $rowActionEdit->setAttributes(['class' => 'btn btn-primary btn-xs m-1']);
        $rowActionEdit->setRouteParameters(['id']);
        $rowActionDelete = new RowAction('Удалить', 'admin_page_delete', true);
        $rowActionDelete->setRouteParameters(['id']);
        $rowActionDelete->setAttributes(['class' => 'btn btn-danger btn-xs m-1']);
        $grid->addRowAction($rowActionEdit);
        $grid->addRowAction($rowActionDelete);
        $grid->getColumn('isVisible')->manipulateRenderCell(function ($value) {
            $icon = 'fa-times';

            if ($value) {
                $icon = 'fa-check';
            }
            return '<i class="fa ' . $icon . '"></i>';
        })->setSafe(false);
        $grid->getColumn('showPreviews')->manipulateRenderCell(function ($value) {
            $icon = 'fa-times';

            if ($value) {
                $icon = 'fa-check';
            }
            return '<i class="fa ' . $icon . '"></i>';
        })->setSafe(false);

        return $grid->getGridResponse('admin/page/index.html.twig');
    }

    public function pageAction(Request $request, Page $page = null)
    {
        if (!$page) {
            $page = new Page();
        }

        $form = $this->createForm(PageFormType::class, $page, [
            'page' => $page,
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Page $page */
            $page = $form->getData();
            foreach ($page->getMedia()->getValues() as $value) {
                $value->setPage($page);
            }

            $this->em->persist($page);
            $this->em->flush();

            return $this->redirectToRoute('admin_page_index');
        }
        return $this->render('admin/page/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function deleteAction(Page $page)
    {
        $this->em->remove($page);
        $this->em->flush();

        return $this->redirectToRoute('admin_page_index');
    }


    public function getMenuPagesAction(Request $request)
    {
        $pages = $this->em->getRepository(Page::class)->getByQuery($request->get('q'));
        $result = [];

        array_map(function ($value) use (&$result){
            /** @var $value Page */
            $result[] = [
                'id' => $value->getId(),
                'text' => $value->getTitle(),
            ];
        }, $pages);

        return new JsonResponse($result, Response::HTTP_OK);
    }

    public function exampleListAction(Request $request)
    {
        $source = new Entity(Example::class);
        $grid = $this->get('grid');
        $grid->setSource($source);
        $rowActionEdit = new RowAction('Редактировать', 'admin_example_edit', false);
        $rowActionEdit->setAttributes(['class' => 'btn btn-primary btn-xs m-1']);
        $rowActionEdit->setRouteParameters(['id']);
        $rowActionDelete = new RowAction('Удалить', 'admin_example_delete', true);
        $rowActionDelete->setRouteParameters(['id']);
        $rowActionDelete->setAttributes(['class' => 'btn btn-danger btn-xs m-1']);
        $grid->addRowAction($rowActionEdit);
        $grid->addRowAction($rowActionDelete);

        return $grid->getGridResponse('admin/example/index.html.twig');
    }

    public function exampleEditAction(Request $request, Example $example = null)
    {
        if (!$example) {
            $example = new Example();
        }

        $form = $this->createForm(ExampleFormType::class, $example, [
            'path' => $this->container->get('assets.packages')->getUrl('/img/main/uploads'),
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $example = $form->getData();
            $this->em->persist($example);
            $this->em->flush();

            return $this->redirectToRoute('admin_example_index');
        }

        return $this->render('admin/example/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function exampleDeleteAction(Example $example)
    {
        $this->em->remove($example);
        $this->em->flush();

        return $this->redirectToRoute('admin_example_index');
    }
}
