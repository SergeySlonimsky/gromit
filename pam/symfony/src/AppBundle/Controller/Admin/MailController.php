<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Settings;
use AppBundle\Repository\SettingsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MailController extends Controller
{
    /** @var \Swift_Mailer */
    private $mailer;

    /** @var EntityManagerInterface */
    private $em;

    /** @var SettingsRepository */
    private $settingsRepository;

    public function __construct(\Swift_Mailer $mailer, EntityManagerInterface $em)
    {
        $this->mailer = $mailer;
        $this->em = $em;
        $this->settingsRepository = $em->getRepository(Settings::class);
    }

    /**
     * @param Request $request
     */
    public function sendAction(Request $request)
    {
        $params = [];
        $content = $request->getContent();
        if (!empty($content)) {
            $params = json_decode($content, true);
        }

        $date = new \DateTime();
        $date = $date->format('d/m/Y H:i');
        $recipientEmail = $this->settingsRepository->findOneBy(['name' => 'email'])->getValue();

        $message = (new \Swift_Message('Заявка с сайта Gromitstroy'))
            ->setFrom($this->getParameter('mailer_user'))
            ->setTo($recipientEmail)
            ->setBody(
                $this->renderView('email/template.html.twig', [
                    'params' => $params,
                    'date' => $date,
                ]),
                'text/html'
            );
        if ($this->mailer->send($message)) {
            return new JsonResponse($this->mailer->send($message), Response::HTTP_OK);
        } else {
            return new JsonResponse($this->mailer->send($message), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function thankAction()
    {
        $recipientPhone = $this->settingsRepository->findOneBy(['name' => 'phone'])->getValue();

        return $this->render('site/thank.html.twig', [
            'phone' => $recipientPhone,
        ]);
    }
}
