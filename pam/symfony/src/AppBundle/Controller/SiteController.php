<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Example;
use AppBundle\Entity\Faq;
use AppBundle\Entity\Page;
use AppBundle\Entity\Settings;
use AppBundle\Entity\Survey\Question;
use AppBundle\Repository\ExampleRepository;
use AppBundle\Repository\PageRepository;
use AppBundle\Repository\SettingsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SiteController extends Controller
{
    /**
     * @var PageRepository
     */
    private $pageRepository;

    /** @var ExampleRepository */
    private $exampleRepository;

    /** @var EntityRepository */
    private $faqRepository;

    /** @var SettingsRepository */
    private $settingsRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->pageRepository = $entityManager->getRepository(Page::class);
        $this->exampleRepository = $entityManager->getRepository(Example::class);
        $this->faqRepository = $entityManager->getRepository(Faq::class);
        $this->settingsRepository = $entityManager->getRepository(Settings::class);
    }

    public function indexAction()
    {
        $items = $this->pageRepository->getPagesByParent();
        $faqs = $this->faqRepository->findAll();
        $mainText = $this->settingsRepository->find('main_text')->getValue();

        return $this->render('site/index.html.twig', [
            'items' => $items,
            'faqs' => $faqs,
            'mainText' => $mainText,
        ]);
    }

    public function pageAction(string $slug)
    {
        $page = $this->pageRepository->getPageBySlug($slug);
        $faqs = $this->faqRepository->findAll();

        return $this->render('site/page.html.twig', [
            'page' => $page,
            'faqs' => $faqs,
        ]);
    }


    public function surveyAction()
    {
        $questions = $this->get('doctrine.orm.default_entity_manager')->getRepository(Question::class)->findAll();

        return $this->render('site/survey.html.twig', [
            'questions' => $questions,
        ]);
    }

    public function getExamplesAction(Request $request)
    {
        $offset = $request->get('offset');

        $examples = $this->exampleRepository->getExamplePart($offset);

        foreach ($examples as $key => $example) {
            $examples[$key]['url'] = '/img/main/uploads/' . $example['url'];
        }

        return new JsonResponse($examples, Response::HTTP_OK);
    }

    public function getMainImagesAction()
    {
        $mainPage = $this->pageRepository->findOneBy(['slug' => '']);
        $result = [];

        foreach ($mainPage->getMedia()->getValues() as $media) {
            $result[] = '/img/main/uploads/' . $media->getUrl();
        }

        return new JsonResponse($result);
    }

    public function notFoundAction()
    {
        return $this->render('site/not-found.html.twig');
    }
}
