<?php

namespace AppBundle\Form;

use AppBundle\Util\FormUtil;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class ProductFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('price', IntegerType::class, [
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('description', CKEditorType::class)
            ->add('categories', Select2EntityType::class, [
                'required' => false,
                'multiple' => true,
                'class' => Category::class,
                'minimum_input_length' => 2,
                'scroll' => false,
                'text_property' => 'name',
                'property' => 'name',
                'remote_route' => 'admin_category_get_by_query',
                'label' => 'Категория',
            ])
            ->add('save', SubmitType::class, FormUtil::SAVE_BTN_OPTS);

        parent::buildForm($builder, $options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
    }
}
