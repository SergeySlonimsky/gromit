<?php

namespace AppBundle\Form;

use AppBundle\Entity\Faq;
use AppBundle\Util\FormUtil;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FaqFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('question', TextType::class, [
                'label' => 'Вопрос',
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('answer', CKEditorType::class)
            ->add('save', SubmitType::class, FormUtil::SAVE_BTN_OPTS);

        parent::buildForm($builder, $options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Faq::class,
        ]);

        parent::configureOptions($resolver);
    }
}
