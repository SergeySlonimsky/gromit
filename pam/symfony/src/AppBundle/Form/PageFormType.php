<?php

namespace AppBundle\Form;

use AppBundle\Entity\Media\Media;
use AppBundle\Entity\Page;
use Doctrine\ORM\EntityRepository;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PageFormType extends ResourceFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Page|null $page */
        $page = $options['page'];
        $builder
            ->add('slug', TextType::class, [
                'empty_data' => '',
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('description', CKEditorType::class)
            ->add('parent', EntityType::class, [
                'class' => Page::class,
                'query_builder' => function (EntityRepository $er) use ($page) {
                    $qb = $er->createQueryBuilder('p');

                    if ($page->getId()) {
                        $qb
                            ->andWhere('p.id != :page')
                            ->andWhere('p.parent != :parent OR p.parent IS NULL')
                            ->setParameter('page', $page->getId())
                            ->setParameter('parent', $page);
                    }

                    return $qb;
                },
                'choice_label' => function (Page $page) {
                    return $page->getTitle();
                },
                'group_by' => function (Page $parent) {
                    if ($parent->isVisible()) {
                        return 'Visible';
                    } else {
                        return 'Not visible';
                    }
                },
                'placeholder' => 'None',
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('isVisible', CheckboxType::class, [
                'required' => false,
                'label' => 'Отображать на сайте',
                'attr' => [
                    'class' => 'form-check-input',
                ]
            ])
            ->add('showPreviews', CheckboxType::class, [
                'required' => false,
                'label' => 'Показывать изображения категории',
                'attr' => [
                    'class' => 'form-check-input',
                ]
            ])
            ->add('media', EntityType::class, [
                'required' => false,
                'class' => Media::class,
                'choice_label' => 'url',
                'expanded' => false,
                'multiple' => true,
            ]);


        parent::buildForm($builder, $options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Page::class,
            'page' => null,
        ]);
    }
}
