<?php

namespace AppBundle\Form;

use AppBundle\Util\FormUtil;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ResourceFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('content', CKEditorType::class)
            ->add('save', SubmitType::class, FormUtil::SAVE_BTN_OPTS);
    }
}
