<?php

namespace AppBundle\Form;

use AppBundle\Entity\Example;
use AppBundle\Entity\Media\Media;
use AppBundle\Util\FormUtil;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExampleFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Заголовок',
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('image', EntityType::class, [
                'label' => 'Изображение',
                'class' => Media::class,
                'choice_label' => 'url',
                'multiple' => false,
                'expanded' => false,
                'choice_attr' => function ($choiceValue) use ($options) {
                    return [
                        'data-img-src' => $options['path'] . '/' . $choiceValue->getUrl(),
                        'data-img-class' => 'example-image-preview',
                    ];
                },
            ])
            ->add('save', SubmitType::class, FormUtil::SAVE_BTN_OPTS);

        parent::buildForm($builder, $options);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Example::class,
            'path' => '',
        ]);
    }
}