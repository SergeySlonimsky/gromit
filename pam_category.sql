
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (1, 1, 'Ограждения лестниц из нержавеющей стали
');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (2, 1, 'Ограждения лестниц с элементами дерева
');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (3, 1, 'Ограждения лестниц со стеклом
');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (4, 1, 'Поручни');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (5, 1, 'Ограждения пандуса
');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (6, 1, 'Отбойники');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (7, 1, 'Ограждения бассейна
');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (8, 1, 'Ограждения балкона
');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (9, 1, 'Другое
');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (10, 1, 'Ограждения не нужны, требуются перегородки');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (11, 2, 'Только на улице
');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (12, 2, 'Только в помещении
');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (13, 2, 'На у лице и в помещении
');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (14, 3, 'Шлифовка');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (15, 3, 'Полировка');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (16, 3, 'Не определились, нужна консультация
');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (17, 4, 'В течение 2-х недель
');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (18, 4, 'В течение месяца
');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (19, 4, 'В течение 3-х месяцев
');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (20, 4, 'Не определились/неизвестно
');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (21, 5, 'C дверями
');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (22, 5, 'Без дверей
');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (23, 5, 'Перегородки не нужны, требуются ограждения
');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (24, 6, 'Прозрачное');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (25, 6, 'Матовое');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (26, 6, 'Бронзовое');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (27, 6, 'Графитовое');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (28, 6, 'Осветленное');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (29, 6, 'Другое');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (31, 8, 'Минск, Минский р-н
');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (32, 8, 'Минская область
');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (33, 8, 'Брестская область
');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (34, 8, 'Витебская область
');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (35, 8, 'Гомельская область
');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (36, 8, 'Гродненская область
');
INSERT INTO pam.survey_answer (id, question_id, answer) VALUES (37, 8, 'Могилевская область
');
INSERT INTO pam.survey_question (id, question, priority, type) VALUES (1, 'Какие виды ограждений вам необходимы?', 1, '');
INSERT INTO pam.survey_question (id, question, priority, type) VALUES (2, 'Где требуется установка?', 2, '');
INSERT INTO pam.survey_question (id, question, priority, type) VALUES (3, 'Какой вид полировки необходим?', 3, '');
INSERT INTO pam.survey_question (id, question, priority, type) VALUES (4, 'Как срочно требуется установка?', 4, '');
INSERT INTO pam.survey_question (id, question, priority, type) VALUES (5, 'Выберите тип перегородок, который вам требуется?', 5, '');
INSERT INTO pam.survey_question (id, question, priority, type) VALUES (6, 'Выберите тип стекла для перегородок', 6, '');
INSERT INTO pam.survey_question (id, question, priority, type) VALUES (7, 'Приблизительные размеры проема, где будут устаналиваться перегородки', 7, '');
INSERT INTO pam.survey_question (id, question, priority, type) VALUES (8, 'Выберите регион в котором находится Ваш объект', 8, '');
INSERT INTO pam.survey_question (id, question, priority, type) VALUES (9, 'Введите ваше имя', 9, '');
INSERT INTO pam.survey_question (id, question, priority, type) VALUES (10, 'Ваш номер телефона', 10, '');
INSERT INTO pam.user (id, username, username_canonical, email, email_canonical, enabled, salt, password, last_login, confirmation_token, password_requested_at, roles) VALUES (1, 'admin', 'admin', 'admin', 'admin', 1, null, '$2y$13$hOygbFJwoR04r6KgBwh0xONkTW1jTOUAeHRN4vkfFFoluMLaIWzAu', '2018-10-08 17:56:28', null, null, 'a:0:{}');