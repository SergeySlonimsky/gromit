-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: db
-- Время создания: Окт 10 2018 г., 23:40
-- Версия сервера: 5.7.23
-- Версия PHP: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `pam`
--

-- --------------------------------------------------------

--
-- Структура таблицы `example`
--

CREATE TABLE `example` (
  `id` int(11) NOT NULL,
  `media_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `example`
--

INSERT INTO `example` (`id`, `media_id`, `title`) VALUES
(1, 11, 'Лестница ул.Велозаводская'),
(3, 12, 'Лестница ул.Велозаводская'),
(4, 13, 'Лестница ул.Велозаводская'),
(5, 14, 'Лестница ул.Велозаводская'),
(6, 15, 'Лестница ул.Велозаводская'),
(7, 16, 'Лестница ул.Велозаводская'),
(8, 17, 'Лестница ул.Велозаводская'),
(9, 18, 'Лестница ул.Велозаводская'),
(10, 19, 'Лестница ул.Велозаводская'),
(11, 20, 'Лестница ул.Велозаводская'),
(12, 21, 'Лестница ул.Велозаводская');

-- --------------------------------------------------------

--
-- Структура таблицы `media_media`
--

CREATE TABLE `media_media` (
  `id` int(11) NOT NULL,
  `page_id` int(11) DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `media_media`
--

INSERT INTO `media_media` (`id`, `page_id`, `url`) VALUES
(8, 3, '76dee47605c2822a1ed898ab8bb35454.jpeg'),
(9, 3, 'eef04d3be2a9f7787753c27ab20884c6.jpeg'),
(10, 3, '952d13a0838062a68dea4cdac7b51167.jpeg'),
(11, 3, 'c1cde39b9f266b7749d47fcb78dd72a6.jpeg'),
(12, 3, '67da29ecb80c4179a9e2d60eb91711b7.jpeg'),
(13, 3, 'af3c4ce817e3aae44e757f175a11bb41.jpeg'),
(14, 3, 'ad7a2bf0829554d654b67b4c826af0ab.jpeg'),
(15, 3, '0978495b300f194de56f862855d54f6c.jpeg'),
(16, 3, '7fc752016c5422a14f4160145837e040.jpeg'),
(17, 3, '23ffeecc1042f51206bc22db26316031.jpeg'),
(18, 3, 'd2c2ba8948ab62c00857895f214f50e7.jpeg'),
(19, 3, 'efb8262e6d3324ba2f50d0ccb0271d6c.jpeg'),
(20, 3, '58811c6874755251fbd7d0582246b1b1.jpeg'),
(21, 3, '53cbfa3cd394495185fa4e3b4faa2a83.jpeg'),
(22, 3, 'e575714cbd02f4a3a431bbd187a04eb8.jpeg'),
(23, 3, '6c013d56ba99c506d02f570d2843b725.jpeg'),
(24, 3, '77fa9436e514cd264861aefbfbc98567.jpeg'),
(25, 3, 'e6b6fff6d05921122b4ba0724c5b3adf.jpeg'),
(26, 3, 'd52b947d92b6fb8be21cbe3b3db81834.jpeg'),
(27, 3, '9326d1787b3c246001838efce69dbe0d.jpeg'),
(28, 3, '68b4d2a05c7e8ec6935b4600f6527f13.jpeg'),
(29, 3, '6a4a97237356efdf31d07e775996da66.jpeg'),
(30, 3, '5eb2517d66a401fee845a91a75d786aa.jpeg'),
(31, 3, '9a518743f73485af67bf3e8ebc702b82.jpeg'),
(32, 3, '20e2c4c0a7ca6c841c425ffb83e17387.jpeg'),
(33, 3, '6dbf77d3ee3dff773d841f8e4628766d.jpeg'),
(34, 3, '78f7b30a4b52153b8fd920031124e1b9.jpeg'),
(35, 3, '880dc613a51be71fd3890d3dd3603582.jpeg'),
(36, 3, '2d7fe541c27e72905a3a43418737347c.jpeg'),
(37, 3, 'a2ec0a3e07a7c6dbd868db3da93d5de8.jpeg'),
(38, 3, '2d1b8a12fb11a606bdaf87ad6b2dfb20.jpeg'),
(39, 3, '213931c1cf0e2f17b591b4447a40b76a.png'),
(40, 3, 'b7ca23f03f03d6f96bc3aa717edd9280.jpeg'),
(41, 3, 'b99ec70505e2b2ab983c1f8a746b7131.jpeg'),
(42, 3, '1805ef76a398bfa25cf02f6b456b8e6c.jpeg'),
(43, 3, 'ad489773a7861d70e826dc6da78ed2f6.jpeg'),
(44, 3, '1fc90f10d7fea71590073a988efea5a8.jpeg'),
(45, 3, '36cef9b42e651f79348a7ad587a4951a.jpeg'),
(46, 3, 'a1f7e1181a9569d2b8822247aa1f95ac.jpeg'),
(47, 3, 'aeefb332745d7c41b91b7fc7f53cae36.jpeg'),
(48, 3, '890dcd454906969a2871ab39a7ca3148.jpeg'),
(49, 3, '4738e19df49baecd06bb1b8e5cdd0b8f.jpeg'),
(50, 3, '59f9cd6d572fea8bc95a35fbaba4d073.jpeg'),
(51, 3, '495febe88e690b4684b826388f4bbe0e.jpeg'),
(52, 3, 'e7cf2685ad3d94b4ec3c23ebec0c6e6a.jpeg'),
(53, 3, '8ff5da9afda11f839bdbc928514176ea.jpeg'),
(54, 3, '58f31dd8ce6e86fbe6a130da96fd3960.jpeg'),
(55, 3, 'ab60f5f9d01c7846fd890d020bbf12d9.jpeg'),
(56, 3, 'aa04dafbd8f6260affa3aa0194cf9ea6.jpeg'),
(57, 3, '26314bd5730676f4b3a3381eef847b24.jpeg'),
(58, 3, '4195a3b713e356d23ef4d865eea9d56a.jpeg'),
(59, 3, '1e4763640139b3dfc9ddd2e7b993e2c8.jpeg'),
(60, 3, '4804733deacf23ce9a6c85d0366e5527.jpeg'),
(61, 3, 'e2c6682678aab49cc0d19c7090b56352.jpeg'),
(62, 3, 'b695a85a2b6c6c2e927e45c0f178025d.jpeg'),
(63, 3, 'c027246537886fdaf406b0246fbbdffb.jpeg'),
(64, 3, '09208ab73fbed76c64752c819785dd87.jpeg'),
(65, 3, '4df1064d4e411f0ed893ddd1230e5339.jpeg'),
(66, 3, 'b90417fa8cf3f248db7f5033aaf4f285.jpeg'),
(67, 3, 'efd2aefac0ad5ddeac0050f28bb42401.jpeg'),
(68, 3, '177ce0397f38709dc1ce42fa4aa59fc2.jpeg'),
(69, 3, '9cc91cc903b315927fc03e068170581e.jpeg'),
(70, 3, 'f3eaed9f9178b8fc87295ffa383e0cf0.jpeg'),
(71, 3, '542a908cd030405c832a44ffdd8a49ca.jpeg'),
(72, 3, '7c92ecfe66da12aea3243c169398937c.jpeg'),
(73, 3, 'ce1121b1ff499b6947e6a53c0049141e.jpeg'),
(74, 3, '42a939e4f66b1227b2a5220113fdd88e.jpeg'),
(75, 3, '5ed1809854a631b889c5de1f7c8d596f.jpeg'),
(76, 3, '35fdf8df78050cf8d7187e525ea41f71.jpeg'),
(77, 4, 'b738efc018c4ca153603f1a6426ee311.jpeg'),
(78, 4, '07ebf669cc5b92077b0dd455e07b16db.jpeg'),
(79, 4, '464d679dbec430ceeb8b7aa1cacdab0c.jpeg'),
(80, 4, '89cfeb469ad97cd9bdbb883e2397a7c7.jpeg'),
(81, 4, 'c6113f7ee2e55ab2cbf39651d8875de9.jpeg'),
(82, 4, '6bc8a10f0f1b19dc9dfcefc85cfa39de.jpeg'),
(83, 4, 'ecf063146c009e88d592fe5852b592a1.jpeg'),
(84, 4, '33e184baad5bf835f4262607b77e3eb0.jpeg'),
(85, 4, 'c9f1ac08069d61bc0767c8b9fdf2b9af.jpeg'),
(86, 4, '14df623c4a5170e8e818a617735fd43d.jpeg'),
(87, 4, '4d6b649afd46358a4204086b5b37c262.jpeg'),
(88, 4, '21b15019e2623330b86fceb3bc7be753.jpeg'),
(90, 4, '1af3d1f868f67020f11e898c157c3899.jpeg'),
(91, 4, '9e0bd19211e4bac45e35f6c09705a99b.jpeg'),
(92, 4, 'fd7e9595daa0effa284fa364d0fe0430.jpeg'),
(93, 4, '47d164873b0028fa7c952225c8ed3c17.jpeg'),
(94, 4, '88c3487ce5ac3fb3702fe752f592c7ef.jpeg'),
(95, 4, 'ae2787ecaf12a16c7816368d04d629bf.jpeg'),
(96, 4, 'e9175f714dfa6dba378782b0e4a055e5.jpeg'),
(97, 4, 'fe23da2c84487e9b811b596a9198e65d.jpeg'),
(98, 4, '678137fa866468a8ab81b749e830a8fb.jpeg'),
(99, 4, 'f644beca250cdc2dfa1fce8385ee5580.jpeg'),
(100, 4, '137e1ae61bf67b0a184b8d0bc6a07218.jpeg'),
(101, 4, 'c2d501298fc541558ec34fd6b0d25aac.jpeg'),
(102, 4, 'e091a5dcf5745827a09bc397b38c255f.jpeg'),
(103, 4, '71acf0b391a0025b55617ea7da9712bf.jpeg'),
(104, 4, '4338892ce226df3904d03bf5206f7c67.jpeg'),
(105, 4, '480381fdd9eacd2d2cec56a521e47731.jpeg'),
(106, 4, '53c0b62eb43d1b846d87130496e9f61e.jpeg'),
(107, 4, 'e10554ee320468a880dd96c05d573779.jpeg'),
(108, 4, '701ccda1856994cea559c8b762076f56.jpeg'),
(109, 4, 'f4a8c5451932716322e389dda3106955.jpeg'),
(110, 4, 'fad800c8531fb0b39ca0703889ab44b6.jpeg'),
(111, 4, 'f5920a2ac22d8c8228772db1e6715f9a.jpeg'),
(113, 4, '1eae42f178dbe999cbd433778aed280d.jpeg'),
(114, 4, '83e439b7fabae16d969e8d607a9e5943.jpeg'),
(115, 4, '2e70ad663080011febf42647f827b86e.jpeg'),
(116, 4, 'b66d774222f1b0882447dbb42a6e8cc2.jpeg'),
(119, 6, '56fcacd45dedc030cd7afd64529d6489.jpeg'),
(120, 6, '324ada451c5157cee679fd32a334849b.jpeg'),
(121, 6, '21159052e486d5cc4c104c5d9c8766f9.jpeg'),
(122, 6, '91c4842c2455a36d14f891dbfe7cfc69.jpeg'),
(123, 6, '4221294fe76bd4eb9f61fbf27c720046.jpeg'),
(124, 6, '8803f8f0253d124a831815a629ae5b22.jpeg'),
(125, 6, '058c1fddbef9663f846550ffc282197e.jpeg'),
(126, 6, 'c6494fded27b8235820c6f85fa376339.jpeg'),
(127, 6, '45b72d7f0b80bde62768f22338015ce1.jpeg'),
(128, 6, '8e6221361aa7005cb30cd1c0c0af3f80.jpeg'),
(129, 6, '3bece1433909364f937e277b79183d06.jpeg'),
(130, 6, 'e0c344478a8562fc896d0f842bda3100.jpeg'),
(131, 6, '1c90eb0779bb8b640e33e14fef8e6e3c.jpeg'),
(132, 6, '80c54524d2362f219d80a1e4b92c3ef1.jpeg'),
(133, 6, 'd1c28ab7fe654d138b4fbf4106bfa76e.jpeg'),
(134, 6, '22fe45afab738669d45c7b2bcd577655.jpeg'),
(135, 6, 'd7af8ae77f0cab38c065562f4e897e0e.jpeg'),
(136, 6, '89791e7c6d07a16301bf6096493ce272.jpeg'),
(137, 6, 'dc379ebcb2bfdc5ed027f6e7ba07982a.jpeg'),
(138, 6, '766c247e3bf29a5482616a563bd6d77c.jpeg'),
(139, 6, '8874a9a2a04fcc3b24be2ccf120027f1.png'),
(140, 6, 'bd8b1280fdb46d345de2274237e12dd2.png'),
(141, 6, '583a5e1a167dfcdae8b102b88ae16949.jpeg'),
(142, 6, 'e64911cdeb7e8ea5ff09a81d4d594adb.jpeg'),
(143, 6, '08e21b6a8826883da2b1d59cf8cd5443.jpeg'),
(144, 6, 'fda5cdb409526a4acd6969f97c928049.jpeg'),
(145, 6, '13cba988a8cf27f4a741558377a36063.jpeg'),
(146, 6, 'd58a62a3069725fe9274ced6fa629da2.jpeg'),
(147, 6, '9d249dc966875ebd41c15061886e30d7.jpeg'),
(148, 6, 'decff11ccaa0df35e07275a534969184.jpeg'),
(149, 6, '1b96c797f01946c342b3c4c5d154a600.jpeg'),
(150, 6, 'f5ca10bbb6fdffc2347907319aa23b6e.jpeg'),
(151, 6, '7c98b8b7ef69e60806343b0e4ec4e83d.jpeg'),
(152, 6, 'b276ff5312738d9cf14d76de7ca08857.jpeg'),
(153, 6, '5c1c1ebf092a943983df777a638d1e09.jpeg'),
(154, 6, '7fa5211ac63ff4b590fccecbbb13d93f.jpeg'),
(155, 6, 'd419b71b32453376cf0481a1e1e05e32.jpeg'),
(156, 6, '2a53a0464defe687949ca038ef2afb2d.jpeg'),
(157, 7, '795711a752ddaff1d35a0f86e1d8fe84.jpeg'),
(159, 7, '0867d4004509561c633c306446877757.jpeg'),
(160, 7, '60ccb70a1dce05557d092a9adb0c7fc5.jpeg'),
(161, 7, '28acc3f3fce058a0e80aeae94fda07ea.jpeg'),
(162, 7, '6e840f8d8851a3fd5605237f5bb1d346.jpeg'),
(163, 7, '138f3d225cbb5a890386bee30cceef61.jpeg'),
(164, 7, 'd03f13de4af8be254272ce64a6a683e7.jpeg'),
(165, 7, 'd5f8c9276312c0672247632fe3765665.jpeg'),
(166, 7, '3853c2e367c8d75be82e7cad1776f7a6.jpeg'),
(167, 7, 'cb90be0342f1c3902efade91bb95b0ab.jpeg'),
(168, 7, 'eaa742ab742971adfddfae80ea6fc64a.jpeg'),
(169, 7, '8b8c2c9713ec3acb4543cf39d5fc58c1.jpeg'),
(170, 7, 'd8bd7ca6fdfcda8d3153550a8c5db16a.png'),
(171, 8, '6c595943041777b1fed5bf359ce261ed.jpeg'),
(172, 8, '7564262140ef8d014df625364e2601ec.jpeg'),
(173, 8, 'a51e9a9150767299c863fee8300a135b.jpeg'),
(174, 8, 'ac21b89a97ebb4310946c85935d9c38d.jpeg'),
(175, 8, '7456aa58610bfd3629bbd8871a2b4c88.jpeg'),
(176, 8, '9324479bb56e6ad8ea477f66a61eae41.jpeg'),
(177, 8, 'aecb6bfef1b7a11ec34de757ed71d190.jpeg'),
(178, 8, 'c0c8bdeef0b401626296b99a120e9854.jpeg'),
(179, 8, '67c9557e30fa6dd978ca14ced1480014.jpeg'),
(180, 8, '11f1fdb50ab0e7fab837a73e6681c17c.jpeg'),
(181, 8, '8c69f0c2f94e4965cc86e0d5b1610a01.jpeg'),
(182, 8, '254d3f62c2cec09ef4bfd1ebe83939e5.jpeg'),
(183, 8, '0193b34c82853ba85599f6621fffb7bb.jpeg'),
(184, 8, 'e08135514f6b1ae78992c95e39a01f28.jpeg'),
(185, 8, '6e231e2ff2eb4d03c4acbed1f4a50a1f.jpeg'),
(186, 8, 'd71398eca1501ca695098047ae2d8f3b.jpeg'),
(187, 8, '71f01fc3e8cb864af40f475ee52510e1.jpeg'),
(188, 8, '2293cbe52466b9233a8b097496019749.jpeg'),
(189, 8, '694261a9edd465acbbb642fb2a040620.jpeg'),
(190, 8, 'cf9ad9fa1a148ede74d37ddc95e06e58.jpeg'),
(191, 8, 'f9f580bd9e41d28a28a0b1cd4b10ec89.jpeg'),
(192, 8, 'f799c6ad9c3abd29dafed4495f9e14e2.jpeg'),
(193, 8, 'bfa25844a793cf54d43f66c65654e324.jpeg'),
(194, 8, 'a4d57105b1f7c7492dd91abaae801f46.jpeg'),
(195, 9, '629563b9fda49c19cbd05945aedcff95.jpeg'),
(196, 9, 'beae9675aa06726e07ed0de43b64b514.jpeg'),
(197, 9, 'b1f8026a42a8363f3800f37e3fa42f4e.jpeg'),
(198, 9, 'a43efc1e807333edab3b0732f2040529.jpeg'),
(199, 9, '6a2dcedb47e28cf2d755037074e58770.jpeg'),
(200, 9, '47c003551be1fdc9274e2ef3dfc88677.jpeg'),
(201, 9, '98b86981b33e97775406c17dd2da3462.jpeg'),
(202, 9, 'd3c3c06042416598fdc86cb73ece3600.jpeg'),
(203, 9, '002299130eeefb6ac30c8c073d9c5b41.jpeg'),
(204, 9, '31af40dcb8f1f83f07a622962cf0c739.jpeg'),
(205, 9, 'b73a702a64a58ca56359bfbb7a015d0a.jpeg'),
(206, 11, '5f0f5955b3f6901d4d00f3b053826410.jpeg'),
(207, 11, '9814bfd4e17b183f88baad7c95d083bc.jpeg'),
(208, 11, '276fd70cf091bdff1a88a7afb150e8fe.jpeg'),
(209, 11, '6a7f35a633266571907a034ea11b90a3.jpeg'),
(210, 11, 'a19a3f554b1086641433164a13eb65b9.jpeg'),
(211, 11, '81cfbaffd54c9c510672d1cdc68f5b65.jpeg'),
(212, 11, 'ffaa93f2a0ade8fa7835619ee014ca60.jpeg'),
(213, 11, '31a31bc40734c52601a80852b0c39c07.jpeg'),
(214, 11, '8cce3b5a9fa351e8b052382b4214b265.jpeg'),
(215, 12, '39bc3c3d703838797033ff9702d97f71.jpeg'),
(216, 12, '80bfd679fafd7141c09d35df1cc26166.jpeg'),
(217, 12, 'aeac069379d2274cf5ac78f25e9923f1.jpeg'),
(218, 12, 'ef028d5680a54cbc08484f8d642466c2.jpeg'),
(219, 12, '729e1eb51d16033c7cac8a08e919d4c1.jpeg'),
(220, 12, '85d66f6cb8d2649aca6afe47a0c1088d.jpeg'),
(221, 12, 'd79dde5ddd4416951724ca756c036978.jpeg'),
(222, 12, '1976dd8bd36fe6b49ceaa8081fb85c11.jpeg'),
(223, 12, 'ddb65dcecdcfcec28f428e52e2c4d7aa.jpeg'),
(224, 13, '332f86dd715a95550f120c5bf4d6ee48.jpeg'),
(225, 13, 'f68734e693a597bd62ed59cb9c7e4b9c.jpeg'),
(226, 13, '2dc6e4123bc7a50f957aeebf58149601.png'),
(227, 13, 'a1591e20098ef7b83c9fbb8c99ca0239.jpeg'),
(228, 13, '1a8f62ce3892724191c6f9b56e8129bf.jpeg'),
(229, 13, '3f71e1312b1d10ba5f207db359b27564.jpeg'),
(230, 13, '15dbd02addcd8b4c72d189df0c8e9539.jpeg'),
(231, 13, '0416d17f596ae2a84e08dbe0ff030377.jpeg'),
(232, 13, 'dd68b273ffbc3c0549278371ea9b49fa.jpeg'),
(233, 15, '28644dcc796e38583eee3ccef6444305.jpeg'),
(234, 15, '2dc44375dd05da06075010be1c1a4ea9.jpeg'),
(235, 15, '32dfebc02cc46cdd33da95329d8d14c1.jpeg'),
(236, 15, '0d15318d80393a58a931084d6ec79879.jpeg'),
(237, 15, '96783702a42787fe718d67d0f75d4e5b.jpeg'),
(238, 15, 'ab48a4320bf735cf883e01977436d1df.jpeg'),
(239, 15, 'cd0c199eddfe84704866b97703f86862.jpeg'),
(240, 15, 'dfdb3b9e41c5fed27edc4352c9370d57.jpeg'),
(241, 15, '60b0aa6ceaa3c0ac5fa7d5ad97c6ac9d.jpeg'),
(242, 16, '6295f57a4f03a6c658cfac020e7e1c6a.jpeg'),
(243, 16, 'efb21ed23767865a719d9d13aab98c83.jpeg'),
(244, 16, '71d3e361dee266206ee9217f6ce7d442.jpeg'),
(245, 16, 'a433ff55e93ed317cfdc6b0414597980.jpeg'),
(246, 16, '1757d618eb4557ae98e9f62f391f7de9.jpeg'),
(247, 16, '7d51cb07f6c37122627408a16373afee.jpeg'),
(248, 16, '90bac11c70ab4b684198377f7d1ffd9e.jpeg'),
(249, 16, '1dee4a0ed8114a76289751832bed5546.jpeg'),
(250, 16, '6b7a53b1f529c16d140ca40db716972c.jpeg'),
(251, 18, 'e462cf7d2338a96178e8b5be3bf33c47.jpeg'),
(252, 18, '038cb43e325ed6159ce30435780028c5.jpeg'),
(253, 18, 'e47701b0b15c8befe8b050bd76903794.jpeg'),
(254, 18, 'dcc039f669f0efe56ac1b523fdbcdd46.jpeg'),
(255, 18, '890700f1239c93afdb8dcb3e9b7ef624.jpeg'),
(256, 18, 'ad6875ab29e0a3a5bf14f08a468b9219.jpeg'),
(257, 18, '1865fa91f3795675f420c7ce429f5514.jpeg'),
(258, 18, 'c09d9b10e2388187c891824123c9c07a.jpeg'),
(259, 18, '0cba20034310cb3795d5a5214ab95efc.jpeg'),
(260, 20, 'e520d860950b0f7fea5853001777f9f1.jpeg'),
(261, 20, '1dbf7b85005d91decdea2af4a177d210.jpeg'),
(262, 20, 'ed726298409507197cf0436ab9e94594.jpeg'),
(263, 20, '285724e4a0fd90633d529d1a681328fc.jpeg'),
(264, 20, 'e7d4f3e68464d2ad98fb123f92e67eb3.jpeg'),
(265, 20, 'e76c36b6c90fa6a626fefa5150c86593.jpeg'),
(266, 20, '2e0a3bcbc41c40b7e1aa3aeb2db046fa.jpeg'),
(267, 20, '783d86b23efd690e144d37dcc973789f.jpeg'),
(268, 20, 'ced37e1465311a011756572ce9521018.jpeg'),
(269, 21, 'a2ae9704379655b6a319c6be167bf626.jpeg'),
(270, 21, '43da7283d7ef55dfe8349d0ace2bc1e4.jpeg'),
(271, 21, 'f010e2feae338aeb17a2007c7dd2ee00.jpeg'),
(272, 21, '1e205dd679a2a59ff396f34c0f6d56be.jpeg'),
(273, 21, '6ec5cfd13bfba720c9a4678b60e4f6a2.jpeg'),
(274, 21, '677ed03c339a895aacc8b6fc6562ac14.jpeg'),
(275, 21, '0b74a2b474ab9b8c10c482743fbbe198.jpeg'),
(276, 21, 'f328887d59b3d86794028d7138dcfcd1.jpeg'),
(277, 21, '0d41e31dd7aafbb999f74a8b1a0b96e2.jpeg'),
(278, 22, '82e8891862f76e1ad7068e635749aa33.jpeg'),
(279, 22, 'f51664e131512c35aa9a901bd487d8c8.jpeg'),
(280, 22, '297359f79d82f06c860248eed03ed0d2.jpeg'),
(281, 22, 'f50e3f37cb12b24f137e8dbd32910527.jpeg'),
(282, 22, '456de0067063fd8abee88d775e692e81.jpeg'),
(283, 22, '17dcc2fb3f4c6b643c8fbb684aba68b8.jpeg'),
(284, 22, '81e785ad402f1ba3ec88747a0dc63d62.jpeg'),
(285, 22, 'b13762692c589a7674c00f7824ab3909.jpeg'),
(286, 22, 'fe04d977a53699a9f4e1149d38182a0d.jpeg'),
(287, 22, '1b92ccb07b49ba1f69b35813b0c36d53.jpeg'),
(288, 22, 'f25c56b3d630c812eee4e982d177e6e5.jpeg'),
(289, 23, 'd3bb3859ee0f00d161bf67acdd476100.jpeg'),
(290, 23, 'a65c9761bfbdec8da7db6ba1bb8f6c07.jpeg'),
(291, 23, '957c3d054f28542e60b61ab132106578.jpeg'),
(292, 23, 'b7c763901017300aeee0be930dc1af0c.jpeg'),
(293, 23, '3825c8597c33e149a115c85edb648823.jpeg'),
(294, 23, '645b8af44a59231573e4b4076d29d021.jpeg'),
(295, 23, '2e1d88e12a8fd347187b9d20ecc9e406.jpeg'),
(296, 23, '294f7f3b07fa537c4e687fae5a9773dc.jpeg'),
(297, 24, '6e50383362e75e25b3f9712ed0920a37.jpeg'),
(298, 24, '3eb761fb2c16deb3b9b892742d80e6ad.jpeg'),
(299, 24, '30755aa56996ed16d086d2b4c62fa390.jpeg'),
(300, 24, '031fbe0d55e19ffc24262d1e75a622ee.jpeg'),
(301, 24, '2e194bf8dc790c4dd299675b62579e45.jpeg'),
(302, 24, '2d83df37c72e0e340e07f146d326a2fd.jpeg'),
(303, 24, '822db0211b9b5168f3de63e09186475f.jpeg'),
(304, 24, '28b033a601ddeff93bc39cde6d5f4e98.jpeg'),
(305, 24, 'e4900efbcfe077042a61b651b827b993.jpeg'),
(306, 26, 'e24cf58ed32392634766095f13afd77d.png'),
(307, 26, 'a9c05afc9d36b4d4c129d8fd7da86ace.jpeg'),
(308, 26, 'd66e565eae95f8b62a280bfa5c86914b.jpeg'),
(309, 26, '4fc65aec4efed35548f6b1613ae782c2.jpeg'),
(310, 26, 'a5a232845206724014f671f1d8dc9974.jpeg'),
(311, 26, '2bc43f1ad0b9cc0a596cb7942cb12449.jpeg'),
(312, 26, '65acce49b689da5b59ecb29f6c50b1a6.jpeg'),
(313, 26, 'fa834ff41e36c6937211e4d2b6c57b1d.jpeg'),
(314, 26, 'bb4f88dd1619d84e481c036dcc6b040c.jpeg'),
(315, 27, 'd65feb4a9390e5b1aa79e41a94d53036.jpeg'),
(316, 27, 'a8f720c7b9988c8abee2089769e427c5.jpeg'),
(317, 27, '58375be97426fef89c5516a35810fd8b.jpeg'),
(318, 27, '0f5d9c4bb9afad18c57e929e26d5f37b.jpeg'),
(319, 27, '664e1866aba97ecfc1e3b62fa03c23a1.jpeg'),
(320, 27, 'c545abca8ee3e6f6eed150d4aa15aeb0.png'),
(321, 27, '48be4ba07c73a8055e015bb4a7aa645e.jpeg'),
(322, 27, '9138cbf10c34d4f1cdee1a3a557e4297.jpeg'),
(323, 27, '8184c4623e14341e1b2cc4f2e13d1c32.jpeg'),
(324, 27, 'c9ff6ddf3f82af517a1b7d627da522c8.jpeg'),
(325, 28, 'be81297f048326ac00531b22769f3b45.jpeg'),
(326, 28, 'eb8a295926a603414d5f6af832c68f68.jpeg'),
(327, 28, '73191932abf59249815a24e0a51994e8.jpeg'),
(328, 28, 'a2b9d8a73cbe54a38a45cd3ca05a4538.jpeg'),
(329, 28, '03d72ea6e73159aa6e8103bca7ff3730.jpeg'),
(330, 28, 'f4c638d6becdb3fed39478ef13b87279.jpeg'),
(331, 28, '264e265be0bf3bf991f436526e0a72be.jpeg'),
(332, 28, '76e3e16b5466569c783685a0163c87af.jpeg'),
(333, 28, '3c6d5aefc81e33d0b03022663c3249b8.jpeg'),
(334, 29, '160f7052eb104516508ab6965d08e47a.jpeg'),
(335, 29, 'bde4c7d992b5d8801e921aa59a5eefff.jpeg'),
(336, 29, 'dc33c23b054a548a6631d2905069fef6.jpeg'),
(337, 29, '3fcb9046505ca4aa03d76d9cc85675d0.jpeg'),
(338, 29, '86203d37d998bc73460213f719c67f5b.jpeg'),
(339, 29, '29a51a4847225348983740035aad391c.jpeg'),
(340, 29, 'c4c72b37f881017f4fd73ee02ec7641f.jpeg'),
(341, 29, '9ab26e081882ecda476952e34b9942da.jpeg'),
(342, 29, 'f63e3cd7dcdb19aa79ac0f545c29d525.jpeg'),
(343, 29, 'a3f2b00b821f951e4225594c541cc5f8.jpeg'),
(344, 30, '0ad6dc5e0052db00b2500e8b01e510f7.jpeg'),
(345, 30, '75f3eceaf23ed510c99cd66c0b65c2e0.jpeg'),
(346, 30, '7da98880101af1cf255263ebc858a2fe.jpeg'),
(347, 30, 'f1e1b0e7a3d3c0428937d1d5d8247917.jpeg'),
(348, 30, '457c8b0125d1be04122493490d42c68a.jpeg'),
(349, 30, 'a048eb45f7abe6767130464d4a833ae0.jpeg'),
(350, 30, '50ee3b2d299797a527caa4c6e612cb77.jpeg'),
(351, 30, '034206d8dbdbca0f1838a3508e7a252d.jpeg'),
(352, 30, 'fb6de77ed34bc1ade61cc689e655e019.jpeg');

-- --------------------------------------------------------

--
-- Структура таблицы `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_visible` tinyint(1) NOT NULL DEFAULT '0',
  `show_previews` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `page`
--

INSERT INTO `page` (`id`, `parent_id`, `title`, `content`, `slug`, `is_visible`, `show_previews`) VALUES
(3, NULL, 'Ограждения из нержавеющей стали', '<h2 style=\"text-align:center\"><strong>Производство и монтаж ограждений</strong></h2>\r\n\r\n<h2 style=\"text-align:center\"><strong>из нержавеющей стали</strong></h2>\r\n\r\n<h3 style=\"text-align:center\">Для физических и юридических лиц. По всей Беларуси.</h3>\r\n\r\n<h3 style=\"text-align:center\">&nbsp;</h3>\r\n\r\n<h3 style=\"text-align:center\">Пройдите простой тест и узнайте предварительную стоимость для вашего объекта</h3>', 'ograzhdenia', 1, 0),
(4, NULL, 'Стеклянные двери и перегородки', '<h2 style=\"text-align:center\"><strong>Производство и монтаж дверей и перегородок из стекла</strong></h2>\r\n\r\n<h3 style=\"text-align:center\">Для физических и юридических лиц. По всей Беларуси</h3>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3 style=\"text-align:center\">Пройдите простой тест и узнайте предварительную стоимость для вашего объекта</h3>', 'peregorodki', 1, 0),
(6, NULL, 'Лестницы', '<h2 style=\"text-align:center\"><strong>Производство и монтаж лестниц</strong></h2>\r\n\r\n<h3 style=\"text-align:center\">Для физических и юридических лиц. По всей Беларуси.</h3>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3 style=\"text-align:center\">&nbsp;</h3>', 'lestnitzi', 1, 0),
(7, NULL, 'Душевый кабины и перегородки', '<h2 style=\"text-align:center\"><strong>Производство и монтаж душевых кабин и перегородок</strong></h2>\r\n\r\n<h3 style=\"text-align:center\">Для физических и юридических лиц. По всей Беларуси.</h3>\r\n\r\n<p>&nbsp;</p>', 'dushevie-kabiny', 1, 0),
(8, NULL, 'Кованые изделия и элементы ковки', '<h2 style=\"text-align:center\"><strong>Производство и монтаж </strong></h2>\r\n\r\n<h2 style=\"text-align:center\"><strong>кованых изделий и элементов ковки</strong></h2>\r\n\r\n<h3 style=\"text-align:center\">Для физических и юридических лиц. По всей Беларуси.</h3>\r\n\r\n<p>&nbsp;</p>', 'kovanie-izdeliya', 1, 0),
(9, NULL, 'Закаленное стекло и зеркала', '<h2 style=\"text-align:center\"><strong>Производство закаленного стекла и зеркал</strong></h2>\r\n\r\n<h3 style=\"text-align:center\">Для физических и юридических лиц. По всей Беларуси.</h3>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>', 'zakalennoe-steklo-i-zerkala', 1, 0),
(11, 3, 'Ограждения лестниц', '<h2 style=\"text-align:center\">Производство и монтаж ограждения лестниц</h2>\r\n\r\n<h3 style=\"text-align:center\">Для физических и юридических лиц. По всей Беларуси.</h3>', 'ograzhdeia-lestniz', 1, 0),
(12, 3, 'Ограждения пандуса', '<h2 style=\"text-align:center\"><strong>Производство и монтаж ограждений пандуса</strong></h2>\r\n\r\n<h3 style=\"text-align:center\">Для физических и юридических лиц. По всей Беларуси.</h3>', 'ograzhdeniya-pandusa', 1, 0),
(13, NULL, 'Отбойники', '<h2 style=\"text-align:center\"><strong>Производство и монтаж отбойников</strong></h2>\r\n\r\n<h3 style=\"text-align:center\">Для физических и юридических лиц. По всей Беларуси.</h3>', 'otboiniki', 0, 0),
(15, 3, 'Лестницы со стеклом', '<h2 style=\"text-align:center\"><strong>Производство и монтаж лестниц со стеклом</strong></h2>\r\n\r\n<h3 style=\"text-align:center\">Для физических и юридических лиц. По всей Беларуси.</h3>', 'lestnizi-so-steklom', 1, 0),
(16, 3, 'Бассейны', '<h2 style=\"text-align:center\">Производство и монтаж ограждений для&nbsp;бассейнов</h2>\r\n\r\n<h3 style=\"text-align:center\">Для физических и юридических лиц. По всей Беларуси.</h3>', 'basseiny', 1, 0),
(18, 3, 'Ограждения балконов', '<h2 style=\"text-align:center\"><strong>Производство и монтаж ограждений балконов</strong></h2>\r\n\r\n<h3 style=\"text-align:center\">Для физических и юридических лиц. По всей Беларуси.</h3>', 'ograzhdeniya-balkonov', 1, 0),
(20, 3, 'Лестницы с элементами дерева', '<h2 style=\"text-align:center\"><strong>Производство и монтаж лестниц с элементами дерева</strong></h2>\r\n\r\n<h3 style=\"text-align:center\">Для физических и юридических лиц. По всей Беларуси</h3>', 'lestnizi-s-elementami-dereva', 1, 0),
(21, 3, 'Поручни', '<h2 style=\"text-align:center\"><strong>Производство и монтаж поручней</strong></h2>\r\n\r\n<h3 style=\"text-align:center\">Для физических и юридических лиц. По всей Беларуси</h3>', 'poruchni', 1, 0),
(22, 3, 'Лестницы из нержавеющей стали', '<h2 style=\"text-align:center\"><strong>Производство и монтаж лестниц из нержавеющей стали</strong></h2>\r\n\r\n<h3 style=\"text-align:center\">Для физических и юридических лиц. По всей Беларуси.</h3>', 'lestnitzi-iz-nerzhaveushey-stali', 1, 0),
(23, 4, 'Маятниковые двери из стекла', '<h2 style=\"text-align:center\"><strong>Производство и монтаж маятниковых дверей из стекла</strong></h2>\r\n\r\n<p style=\"text-align:center\">Для физических и юридических лиц. По всей Беларуси.</p>', 'mayatnikovye-dveri', 1, 0),
(24, 4, 'Межкомнатные двери из стекла', '<h2 style=\"text-align:center\"><strong>Производство и монтаж межкомнатных дверей из стекла</strong></h2>\r\n\r\n<h3 style=\"text-align:center\">Для физических и юридических лиц. По всей Беларуси</h3>', 'mezhkomnatnie-dveri-iz-stekla', 1, 0),
(26, 4, 'Распашные двери из стекла', '<h2 style=\"text-align:center\"><strong>Производство и монтаж распашных дверей из стекла</strong></h2>\r\n\r\n<h3 style=\"text-align:center\">Для физических и юридических лиц. По всей Беларуси.</h3>', 'raspashnie-dveri-iz-stekla', 1, 0),
(27, 6, 'Консольные лестницы', '<h2 style=\"text-align:center\"><strong>Произдвоство и монтаж консольных лестниц</strong></h2>\r\n\r\n<h3 style=\"text-align:center\">Для физических и юридических лиц. По всей Беларуси.</h3>', 'konsolnie-lestnizy', 1, 0),
(28, 6, 'Лестницы на металлическом каркасе', '<h2><strong>Производство и монтаж лестниц</strong></h2>\r\n\r\n<h4>Текст 2</h4>', 'lestnizi-na-metallicheskom-karkase', 0, 0),
(29, 6, 'Модульные лестницы', '<h2 style=\"text-align:center\"><strong>Производство и монтаж лестниц</strong></h2>\r\n\r\n<h4 style=\"text-align:center\">Текст 2</h4>', 'modulnie-lestnizi', 0, 0),
(30, 6, 'Стеклянные лестницы', '<h2 style=\"text-align:center\"><strong>Производство и монтаж лестниц</strong></h2>\r\n\r\n<h4 style=\"text-align:center\">Текст 2</h4>', 'stekliannie-lestnizi', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE `settings` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`name`, `value`) VALUES
('email', 'test'),
('phone', '+375295860452');

-- --------------------------------------------------------

--
-- Структура таблицы `survey_answer`
--

CREATE TABLE `survey_answer` (
  `id` int(11) NOT NULL,
  `question_id` int(11) DEFAULT NULL,
  `answer` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `survey_answer`
--

INSERT INTO `survey_answer` (`id`, `question_id`, `answer`) VALUES
(1, 1, 'Ограждения лестниц из нержавеющей стали'),
(2, 1, 'Ограждения лестниц с элементами дерева'),
(3, 1, 'Ограждения лестниц со стеклом'),
(4, 1, 'Поручни'),
(5, 1, 'Ограждения пандуса'),
(6, 1, 'Отбойники'),
(7, 1, 'Ограждения бассейна'),
(8, 1, 'Ограждения балкона'),
(9, 1, 'Другое'),
(11, 2, 'Только на улице\n'),
(12, 2, 'Только в помещении\n'),
(13, 2, 'На у лице и в помещении\n'),
(14, 3, 'Шлифовка'),
(15, 3, 'Полировка'),
(16, 3, 'Не определились, нужна консультация\n'),
(17, 4, 'В течение 2-х недель\n'),
(18, 4, 'В течение месяца\n'),
(19, 4, 'В течение 3-х месяцев\n'),
(20, 4, 'Не определились/неизвестно\n'),
(21, 5, 'C дверями\n'),
(22, 5, 'Без дверей\n'),
(23, 5, 'Перегородки не нужны, требуются ограждения\n'),
(24, 6, 'Прозрачное'),
(25, 6, 'Матовое'),
(26, 6, 'Бронзовое'),
(27, 6, 'Графитовое'),
(28, 6, 'Осветленное'),
(29, 6, 'Другое'),
(31, 8, 'Минск, Минский р-н\n'),
(32, 8, 'Минская область\n'),
(33, 8, 'Брестская область\n'),
(34, 8, 'Витебская область\n'),
(35, 8, 'Гомельская область\n'),
(36, 8, 'Гродненская область\n'),
(37, 8, 'Могилевская область\n'),
(38, NULL, 'Test1'),
(39, NULL, 'Test1'),
(40, 1, 'Test1');

-- --------------------------------------------------------

--
-- Структура таблицы `survey_question`
--

CREATE TABLE `survey_question` (
  `id` int(11) NOT NULL,
  `question` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `priority` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `survey_question`
--

INSERT INTO `survey_question` (`id`, `question`, `priority`, `type`) VALUES
(1, 'Какие виды ограждений вам необходимы?', 0, 'ograzhdeniya'),
(2, 'Где требуется установка?', 1, 'ograzhdeniya'),
(3, 'Какой вид полировки необходим?', 2, 'ograzhdeniya'),
(4, 'Как срочно требуется установка?', 3, NULL),
(5, 'Выберите тип перегородок, который вам требуется?', 4, 'peregorodki'),
(6, 'Выберите тип стекла для перегородок', 5, 'peregorodki'),
(7, 'Приблизительные размеры проема, где будут устаналиваться перегородки', 6, 'peregorodki'),
(8, 'Выберите регион в котором находится Ваш объект', 7, NULL),
(9, 'Введите ваше имя', 8, NULL),
(10, 'Введите ваш телефон', 9, '');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`) VALUES
(1, 'admin', 'admin', 'admin', 'admin', 1, NULL, '$2y$13$hOygbFJwoR04r6KgBwh0xONkTW1jTOUAeHRN4vkfFFoluMLaIWzAu', '2018-10-10 23:04:22', NULL, NULL, 'a:0:{}');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `example`
--
ALTER TABLE `example`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_6EEC9B9FEA9FDD75` (`media_id`);

--
-- Индексы таблицы `media_media`
--
ALTER TABLE `media_media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_753565BDC4663E4` (`page_id`);

--
-- Индексы таблицы `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_140AB620727ACA70` (`parent_id`);

--
-- Индексы таблицы `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`name`);

--
-- Индексы таблицы `survey_answer`
--
ALTER TABLE `survey_answer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F2D382491E27F6BF` (`question_id`);

--
-- Индексы таблицы `survey_question`
--
ALTER TABLE `survey_question`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D64992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_8D93D649A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_8D93D649C05FB297` (`confirmation_token`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `example`
--
ALTER TABLE `example`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `media_media`
--
ALTER TABLE `media_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=353;

--
-- AUTO_INCREMENT для таблицы `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT для таблицы `survey_answer`
--
ALTER TABLE `survey_answer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT для таблицы `survey_question`
--
ALTER TABLE `survey_question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `example`
--
ALTER TABLE `example`
  ADD CONSTRAINT `FK_6EEC9B9FEA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `media_media` (`id`);

--
-- Ограничения внешнего ключа таблицы `media_media`
--
ALTER TABLE `media_media`
  ADD CONSTRAINT `FK_753565BDC4663E4` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`);

--
-- Ограничения внешнего ключа таблицы `page`
--
ALTER TABLE `page`
  ADD CONSTRAINT `FK_140AB620727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `page` (`id`);

--
-- Ограничения внешнего ключа таблицы `survey_answer`
--
ALTER TABLE `survey_answer`
  ADD CONSTRAINT `FK_F2D382491E27F6BF` FOREIGN KEY (`question_id`) REFERENCES `survey_question` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
